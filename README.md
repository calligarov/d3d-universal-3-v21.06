# D3D Universal 3 v21.06

Repository for the main assembly of [Open Source Ecology D3D Universal 3 v21.06 3D Printer](https://wiki.opensourceecology.org/wiki/D3D_Universal_v21.06)

# License
CC BY SA

# Usage

To open this file download and install [FreeCAD 0.16](https://github.com/FreeCAD/FreeCAD/releases/tag/0.16) or newer
